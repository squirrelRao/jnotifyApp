#! /bin/bash

clear
. ./para

export LANG=zh_CN.gb18030
echo $LANG

CLASSPATH=
CLASSPATH=$CLASSPATH:${CLIENT_BIN}
for jarfile in `ls -1 ${CLIENT_LIB}/*.jar`
do
    CLASSPATH="$CLASSPATH:$jarfile"
done

cd ${CLIENT_BIN}
echo "${CLIENT_NAME} starting ..."
sleep 1
nohup java -Xms256m -Xmx512m -Dflag=${CLIENT_NAME} -XX:PermSize=128m -XX:MaxPermSize=256m -Djava.security.policy="policy.txt" -Djava.rmi.server.codebase=file://${CLIENT_BIN}/ -cp ${CLASSPATH} com.umpay.provdec.Provdec >${CLIENT_LOG}/console.out &

cd ${CLIENT_SCRIPT}
#sleep 1
tail -50f ${CLIENT_LOG}/console.out
