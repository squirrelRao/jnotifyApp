package core;

import entity.Target;
import net.contentobjects.jnotify.JNotify;
import net.contentobjects.jnotify.JNotifyListener;
import util.PropertiesUtil;

import java.io.*;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;

/**
 * Created with IntelliJ IDEA.
 * User: squirrelrao
 * Date: 13-10-3
 * Time: ����7:01
 * To change this template use File | Settings | File Templates.
 */
public class FileListener {

    public void listen() throws Exception {


        List<Target> targets = this.getListenTargets();

        File outputFile = new File("output/monitor.out");

        if(!outputFile.exists()) {
            outputFile.createNewFile();
        }

        for(int i = 0; i < targets.size(); i++) {


            Target target = targets.get(i);

            JNotify.addWatch(target.getFile(),this.getMask(target.getListenItem()),true,new JNotifyListener() {

                public void fileRenamed(int wd, String rootPath, String oldName, String newName)
                {
                    String content = "file renamed : wd #" + wd + " root = " + rootPath + ", " + oldName + " -> " + newName;
                    this.recordMonitor(content);
                }

                public void fileModified(int wd, String rootPath, String name)
                {
                    String content = "file modified : wd #" + wd + " root = " + rootPath + ", " + name;
                    this.recordMonitor(content);

                }

                public void fileDeleted(int wd, String rootPath, String name)
                {
                    String content =  "file deleted : wd #" + wd + " root = " + rootPath+ ", " + name;
                    this.recordMonitor(content);

                }

                public void fileCreated(int wd, String rootPath, String name)
                {
                    String content = " file created : wd #" + wd + " root = " + rootPath+ ", " + name;
                    this.recordMonitor(content);

                }

                private void recordMonitor(String content)  {

                    System.out.println(content);
                    BufferedWriter out = null;
                    try {
                        out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("output/monitor.out", true)));
                        out.write(content);
                        out.write("\n");

                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        try {
                            if(out != null){
                                out.close();
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });

            System.out.println("start listen file:"+target.getFile()+"  @"+target.getListenItem());

        }

        while(true) {

            Thread.sleep(1000000);
        }

    }

    private int getMask(String listenItem) {


         int mask = 0;

         if(listenItem == null || listenItem.trim().equals("")) {
             return mask;
         }

         for(int i = 0; i < listenItem.length(); i++) {

            char item = listenItem.charAt(i);

            if(item == 'c') {   //create file

                mask |= JNotify.FILE_CREATED;

            } else  if(item == 'd') { //delete file

                mask |= JNotify.FILE_DELETED;

            } else  if(item == 'm') {  //modify file

                mask |= JNotify.FILE_MODIFIED;

            } else  if(item == 'r') { // rename file

                mask |= JNotify.FILE_RENAMED;

            }

         }


         return mask;

    }



    public List<Target> getListenTargets() throws Exception {

        Properties properties = PropertiesUtil.getProperties("config/listen.properties");

        Enumeration names =  properties.propertyNames();

        List<Target> targtes = new ArrayList<Target>();

        while(names.hasMoreElements()) {

            Object value = properties.get(names.nextElement());

            if(value == null){
                continue;
            }


            String[] values = ((String)value).split(",");

            Target target = new Target();
            target.setListenItem(values[0]);
            target.setFile(values[1]);

            targtes.add(target);

        }
        return targtes;
    }

}
