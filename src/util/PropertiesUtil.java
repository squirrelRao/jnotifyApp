package util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * 此处填写功能、用途等，如果多行，用<br>换行
 * <p>创建日期：Jul 12, 2012 </p>
 *
 * @version:
 * @author: 饶平平
 * @see
 */
public class PropertiesUtil {

    public volatile static String SEQ_PROPERTIES = "seq.properties";

    /**
     * 获取Property属性
     * <p>创建人：饶平平 ,  Nov 16, 2012  10:59:46 AM</p>
     * <p>修改人：饶平平 ,  Nov 16, 2012  10:59:46 AM</p>
     *
     * @param path
     * @param key
     * @return
     * @throws Exception
     */
    public static int getIntProperty(String path, String key) {

        Properties properties = null;
        int property = 0;
        try {
            properties = PropertiesUtil.getProperties(path);

            if (properties == null) {

                return -1;
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return property;
        }

        String value = properties.getProperty(key);
        if (value != null && !"".equals(value)) {

            property = Integer.valueOf(value).intValue();
        }

        return property;
    }


    /**
     * 获取指定路径的properties文件
     * <p>创建人：饶平平 ,  Jul 16, 2012  10:28:12 AM</p>
     * <p>修改人：饶平平 ,  Jul 16, 2012  10:28:12 AM</p>
     *
     * @param path
     * @return
     * @throws Exception
     */
    public static String getProperty(String path, String key) throws Exception {

        Properties properties = null;
        String property = null;
        try {
            properties = PropertiesUtil.getProperties(path);

            if (properties == null) {

                return null;
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return property;
        }

        return properties.getProperty(key);
    }

    /**
     * 获取指定路径的properties文件
     * <p>创建人：饶平平 ,  Jul 16, 2012  10:28:12 AM</p>
     * <p>修改人：饶平平 ,  Jul 16, 2012  10:28:12 AM</p>
     *
     * @param path
     * @return
     * @throws Exception
     */
    public static Properties getProperties(String path) throws Exception {

        Properties properties = new Properties();

        File file = new File(path);
        if (!file.exists()) {
            System.out.println("提示：配置文件不存在! " + path);
            return null;
        }

        FileInputStream in = new FileInputStream(new File(path));
        try {
            properties.load(in);
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
            in.close();
            throw e;

        } finally {

            if (in != null) {
                in = null;
            }
        }

        return properties;
    }

}
