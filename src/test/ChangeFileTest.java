package test;

import core.FileListener;
import entity.Target;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.List;
import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * User: squirrelrao
 * Date: 13-10-4
 * Time: ����9:10
 * To change this template use File | Settings | File Templates.
 */
public class ChangeFileTest {


    public static void main(String[] args)  throws  Exception{

        int count = 10;

         ChangeFileTest test = new ChangeFileTest();

         while(true) {

         for(int i = 0; i < count; i++) {

               test.getTestThread().run();

          }


             Thread.sleep(1000);
         }


    }

    public TestThread getTestThread() {


        return new TestThread();
    }

    private class TestThread extends Thread {


        public void run() {

            List<Target> targets = null;
            try {
                targets = (new FileListener()).getListenTargets();
            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }

            int operate = (new Random()).nextInt(4);
            String fileName = "test"+String.valueOf((new Random()).nextInt(100)) ;
            Target target = targets.get((new Random().nextInt(targets.size())));

            if(target == null){

                return;
            }

            System.out.println(fileName+" operate:"+operate);

            File dir = new File(target.getFile());
            String[] files = dir.list(new FileFilter());

            if(operate == 0) {   //create file

                     File file = new File(target.getFile()+"/"+fileName);
                    try {
                        file.createNewFile();
                    } catch (IOException e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }

                   System.out.println("create file :"+file.getAbsolutePath());

            } else if (operate == 1) {   //modify file

               if(files != null && files.length > 0) {

                   File file = new File(target.getFile()+"/"+files[(new Random()).nextInt(files.length)]);
                   file.setLastModified(System.currentTimeMillis());
                   System.out.println("modify file :"+file.getAbsolutePath());


               }




            } else if (operate ==2) {   //rename file

                if(files != null && files.length > 0) {

                    File file = new File(target.getFile()+"/"+files[(new Random()).nextInt(files.length)]);

                    file.renameTo(new File(file.getAbsolutePath()+"new"));
                    System.out.println("rename file :"+file.getAbsolutePath());

                }

            } else if(operate == 3) {    //delete file


                if(files != null && files.length > 0) {

                    File file = new File(target.getFile()+"/"+files[(new Random()).nextInt(files.length)]);
                    if(file.delete()){

                        System.out.println("delete success");
                    }else{
                        System.out.println("delete failure");


                    }


                    System.out.println("delete file :"+file.getAbsolutePath());

                }

            }

        }


        private class  FileFilter implements FilenameFilter {


            @Override
            public boolean accept(File file, String s) {

                  if(s.substring(0,4).trim().equalsIgnoreCase("test")){
                      return true;
                  }

                  return false;

            }
        }
    }
}
